﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {


    //Variables

    public bool Empty = true;
    public int NumberObjects = 3;
    public GameObject Item;

    

    void OnTriggerEnter2D(Collider2D collision) 
    {
        if(collision.gameObject.tag == "MarioHead" && Movimiento.Transformation >= 2) 
        {
            if(!Empty && NumberObjects != 0)
            {
                NumberObjects--;
                Instantiate(Item, new Vector2(transform.position.x, transform.position.y + 0.4f), Quaternion.identity);
            }
            else if(Empty)
            {
                Destroy(gameObject);
            }
        }
    }
}
