﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fall : MonoBehaviour {

    GameObject Mario;
    private bool AnimationDead = false;
    

    //Animacion de Mario al caer

    private void OnCollisionEnter2D(Collision2D collision)
    {
       
        if (collision.gameObject.tag == "Player")
        {
            if(!AnimationDead)
            {
                Mario.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 300));
                Movimiento.Dead = true;
                while(Movimiento.Transformation != 1)
                {
                    Movimiento.Transformation--;
                    Mario.GetComponent<Animator>().SetInteger("Transformation", Movimiento.Transformation);
                }
                Mario.GetComponent<Animator>().SetBool("Dead", true);
                Mario.GetComponent<BoxCollider2D>().size = new Vector2(0.15f, 0.2f);
                AnimationDead = true;
                Movimiento.Live--;
            }
            else
            {
                Mario.transform.localScale = new Vector3(3, 3, 3);
                Mario.transform.position = Movimiento.Spawn;
                Movimiento.Dead = false;
                Mario.GetComponent<Animator>().SetBool("Dead", false);
                AnimationDead = false;
            }
            
            
        }
    }

    private void Start()
    {
        Mario = GameObject.Find("Mario");
    }
}
