﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour {


    public static int Coins = 0;
    public float speed;
    public float JumpForce = 100f;
    public static bool Dead = false;
    bool Ground;
    public static bool Invencible = false;
    public static bool Hitted = false;
    Animator myAnimator;
    public static int Live = 3;
    public static Vector3 Spawn;
    public static int Transformation = 1;
    public ParticleSystem dust;
    public static float TimeInvencible = 0.0f;

    //SistemaParticulas
    void DustPlay()
    { 
        dust.Play();
    }
    void DustStop()
    {
        dust.Stop();
    }

    void Start()
    {
        myAnimator = GetComponent<Animator>();
        Ground = true;
        DustPlay();
    }

    //Animacion saltar

    public void Jump()
    {
        DustStop();
        Ground = false;
        myAnimator.SetBool("jump", true);
        GetComponent<Rigidbody2D>().AddForce(new Vector2(0, JumpForce));

    }
    
    //Colisiones
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag==("Grass") || collision.gameObject.tag == ("Bloque") || collision.gameObject.tag == ("BloquePregunta"))
        {
            Ground = true;
            myAnimator.SetBool("jump", false);
            
        }
      
    }
	
	void Update ()
    {


        if(Live <= 0)
        {
            GameOver.show();
        }


        //Movimiento con teclado
        if (Input.GetKey(KeyCode.D) && !Dead)
        {
            transform.localScale = new Vector3(3, 3, 3); 
            transform.Translate(Time.deltaTime * speed, 0, 0);
            myAnimator.SetFloat("walk", speed);
            
        }
           

        if (Input.GetKey(KeyCode.A) && !Dead)
        {
            transform.localScale = new Vector3(-3, 3, 3);
            transform.Translate(-Time.deltaTime * speed, 0, 0);
            myAnimator.SetFloat("walk", Mathf.Abs(speed));
            
        }


        if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
        {
            myAnimator.SetFloat("walk", 0);
            DustPlay();

            
        }

        //Salto
        if(Input.GetKeyDown(KeyCode.Space) && Ground == true && !Dead)
        {
            Jump();

        }
    }

    public IEnumerator HittedMode()
    {
        if(Transformation >= 2)
        {
            Transformation--;
            myAnimator.SetInteger("Transformation", Transformation);
            Invencible = true;
            yield return new WaitForSeconds(2);
            Invencible = false;
        }
        else
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 300));
            GetComponent<Animator>().SetBool("Dead", true);
            yield return new WaitForSeconds(5);
            transform.localScale = new Vector3(3, 3, 3);
            transform.position = Spawn;
            Dead = false;
            GetComponent<Animator>().SetBool("Dead", false);
            Live--;
        }
    }
}
