﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionBlock : MonoBehaviour {

    Animator myQuestionBlockAnimator;
    public GameObject Item;
    private bool Hitted = false;

    //Colisiones bloque de pregunta
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if(!Hitted)
            {
                Hitted = true;
                myQuestionBlockAnimator.SetBool("hitted", Hitted);
                Instantiate(Item, new Vector2(transform.position.x, transform.position.y+0.4f), Quaternion.identity);
            }
        }
        
    }

    void Start ()
    {
        myQuestionBlockAnimator = GetComponent<Animator>();
        
	}
	
	
	void Update () {
		
	}
}
