﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    //Variables
    public static int GoombaLive = 1;
    public float GoombaSpeed;
    public float distance;
    private bool movingRight = true;
    public Transform groundDetection;

    Animator myAnimator;
    GameObject Mario;

    void Start()
    {
        Mario = GameObject.Find("Mario");
        myAnimator = GetComponent<Animator>();   
        
    }

    //Animaciones
    void IsLive()
    {
        if(GoombaLive == 0) 
        {
            myAnimator.SetBool("Dead", true);
            Destroy(gameObject);
        }
    }

    //Colisiones
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if(Movimiento.Invencible)
            {
                Destroy(gameObject);
            }
            else if(!Movimiento.Hitted)
            {
                if (Movimiento.Transformation >= 2)
                {
                    Movimiento.Transformation--;
                    Mario.GetComponent<Animator>().SetInteger("Transformation", Movimiento.Transformation);
                    Movimiento.Hitted = true;
                }
                else
                {
                    Mario.transform.localScale = new Vector3(3, 3, 3);
                    Mario.GetComponent<BoxCollider2D>().size = new Vector2(0.15f, 0.2f);
                    Mario.transform.position = Movimiento.Spawn;
                    Movimiento.Live--;
                }
            }
            
        }
    }
    void Update()
    {
        if(Movimiento.Hitted)
        {
            Movimiento.TimeInvencible += Time.deltaTime;
        } 
        if (Movimiento.TimeInvencible >= 2.0f)
        {
            Movimiento.Hitted = false;
            Movimiento.TimeInvencible = 0.0f;
        }

        IsLive();
        transform.Translate(Vector2.right * GoombaSpeed * Time.deltaTime);
        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, distance);
        if (groundInfo.collider == false)
        {
            if(movingRight == true)
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                movingRight = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingRight = true;
            }
        }
        
    }
}
