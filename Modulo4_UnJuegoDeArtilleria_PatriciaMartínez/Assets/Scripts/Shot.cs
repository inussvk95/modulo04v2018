﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Shot : MonoBehaviour {

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == ("Bloque") || collision.gameObject.tag == ("BloquePregunta"))
        {
            Destroy(gameObject);
        }
        else if(collision.gameObject.tag == ("Enemy"))
        {
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
    }
}
